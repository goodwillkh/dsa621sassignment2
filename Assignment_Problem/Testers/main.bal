import ballerina/io;
import ballerina/file;
import ballerina/lang.value;
//import ballerina/lang.array;
//import ballerina/log;


public type Object record {|
    string keys;
    string fileName;
    string...; // takes different types of strings
    
|};

public enum CommitStatus {
    SUCCESS,FAIL
}
public string jsonFilePath2 = "./filesToCommit/Testfile.json";   // this will be the test file to calculate the size
public string jsonFilePath = "./files/jsonFile.json";      // this will hbe the file that will be pushed to actual storage

public string Obj = "new";
public int k = 1;
// creating a function to handle file commits
// taakes in the to commit file size and the file to commit
public function fileControl(string m, json c) returns string|error{
        // we will only compare with the current file in the commit path
        // if the file exist it will be replaced 
        // if the file does not exit it will create a new file.m

        // lets first get the key for the new file
        string keyval = check value:ensureType(c.keys, string);

        if (m == keyval){
            io:println("warning the file with key ",m, "already exist and will be replaced");

             // now to wrtie the file
            check io:fileWriteJson(jsonFilePath, c);
        }
        else {
            // create a new file path
            io:println("This is a new key value and a new file will be saved");
            anydata fname = Obj; 
            fname = fname.toString() + k.toString();
            string fnameN = <string> fname;
            io:println(fnameN);
            k = k+1; 
            string jsonFilePathN = "./files/"+fnameN+".json";      // this will hbe the file that will be pushed to actual storage
            //io:println("The new file path is : \n",jsonFilePathN); // this is to test file path name
            //io:println("The new k is : \n",k); this is to check if k will change for next use

            // now to wrtie the file
            check io:fileWriteJson(jsonFilePathN, c);
        }

        string msg = "The key not found"; 
        return msg;
}

public function getFileMeta() returns string|error {
        // looping through the directory
    do {
	        // looping through the directory
	    file:MetaData[] readDirResultsw = check file:readDir("files");
        int count = 0;
        json meta = {};
        foreach var item in readDirResultsw {
        json m1 = {"Directory path: ": readDirResultsw[count].absPath};
        json m2 = {"Directory size: ": readDirResultsw[count].size.toString()};
        json m3 = {"Is directory: ": readDirResultsw[count].dir.toString()};
        json m4 = {"Modified at ": readDirResultsw[count].modifiedTime.toString()};
        
        io:println("This was the file in pos", count);   
        io:println("");
        io:println("");
        count += 1;
        json mx = check m1.mergeJson(m2);
        json my = check m3.mergeJson(m4);
        meta = check  mx.mergeJson(my);
        }
        return meta.toString();
    } on fail var e {
    	return e;
    }
    
    }

    


# Description
# + return - Return Value Description  
public function main() returns error? {
    // Initializes the JSON file path and content.
    // this will create a folder called files and a file called jsonFile.json


    // this needs to be sent by the client
    // creates a sampe json to store in the jsonfile
    json jsonContent = {keys:"Obj2", fileName:"SampleFile",
    "Store": {
            "@id": "AST",
            "name": "Anne",
            "address": {
                "street": "Main",
                "city": "94"
            },
            "codes": ["4", "8"]
        }};
         

        // the file writing process
        check io:fileWriteJson(jsonFilePath, jsonContent);
        

        //this needs to be send by the client
        //creates a record object of type json
        Object mm = {keys:"Obj1", fileName:"SampleFile",
    "Store":"This is the message that will be stored as a  json object and can be used over and over again in the file" };
       
        // type cast object to json type
        json objectmsg = mm.toJson();

        // Writes the given JSON to a file.
        check io:fileWriteJson(jsonFilePath2, objectmsg);
        io:println("");
        
        // If the write operation was successful, then, performs a read operation to read the JSON content.
        json readJson2 = check io:fileReadJson(jsonFilePath2);
        io:println(readJson2);
        io:println("Printing only the key value in the above json");

        //Printing only the key value in the above json
        string s2 = check value:ensureType(readJson2.keys, string);
        io:println(s2);
        io:println("");

    // this will check all files and get file size before commiting 
    file:MetaData[] readDirResults = check file:readDir("filesToCommit");
    int fileSize = readDirResults[0].size;
    int fileSize2 = check value:ensureType(readDirResults[0].size, int);
    //string oldkey = check value:ensureType(readDirResults[0].keys(), string);
    // testing the file sizes again

    io:println("The file has this size:", fileSize2);
    
     // checks if file size is <=30mb then writes a file 30 MiB = 31457280 Bytes
    if fileSize2 <= 31457280 {
     // call the method here to do the checkng of the key
        string|error  resullts = fileControl(s2, objectmsg);
        
    }
    else{
        // error message
        io:println("The file size exceeds 30mb ");
        // prints the file size
        io:println("The size of the file is: ",fileSize2);
    }


    io:println("");



    io:println(getFileMeta());


     
}
